// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App'
import Home from './components/home/Home.vue'
import User from './components/User.vue'
import Wait from './components/Wait.vue'

Vue.use(VueRouter)

const routes = [
	{
	  path: '/',
	  component: Home
	},
	{
		path:'/user',
		component:User
	},{
		path:'/wait',
		component:Wait
	}
]

const router = new VueRouter({
  mode:'history',
  routes
})

FastClick.attach(document.body)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box')
